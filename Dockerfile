FROM python:3.8
WORKDIR /usr/src/app
COPY . .
RUN pip install -r requirements.txt
HEALTHCHECK --interval=1s --retries=10 \
  CMD curl -f 'http://localhost:5001/healthcheck'

CMD ["python3", "app.py"]
