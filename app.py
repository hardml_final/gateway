import os
import pickle

import numpy as np
import requests
from flask import Flask, jsonify, request

app = Flask(__name__)

EMB_SERVING_URL = os.environ['EMB_SERVING_URL']
CENTROIDS_PATH = os.environ['CENTROIDS_PATH']
KNN_WORKER_HOST = os.environ['KNN_WORKER_HOST']
KNN_WORKER_START_PORT = int(os.environ['KNN_WORKER_START_PORT'])
KNN_WORKER_ENDPOINT = os.environ['KNN_WORKER_ENDPOINT']

with open(CENTROIDS_PATH, 'rb') as f:
    centroids: dict = pickle.load(f)
    # если не подгрузились центроиды, прямо здесь и упадем, что есть ожидаемое поведение


def get_text_embedding(text):
    emb_serving_resp = requests.post(url=EMB_SERVING_URL, json={"inputs": [text]}).json()
    emb = emb_serving_resp['outputs'][0]
    return np.array(emb)


def cosine_sim(a, b):
    return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


def get_closest_cluster_idx(emb: np.ndarray):
    # здесь ищем номер ближайшего кластера
    max_sim = -999
    closest_idx = None
    for cluster_idx, cluster_center_emb in centroids.items():
        sim = cosine_sim(cluster_center_emb, emb)
        if sim > max_sim:
            closest_idx = cluster_idx
            max_sim = sim
    return closest_idx


def get_knn_worker_url(cluster_idx, n_neighbors=5):
    # по номеру кластера собираем url knn-воркера, чтобы сходить за ближайшими соседями
    port = KNN_WORKER_START_PORT + int(cluster_idx)
    url = f'http://{KNN_WORKER_HOST}:{port}{KNN_WORKER_ENDPOINT}?n_neighbors={n_neighbors}'
    return url


def get_knn_resp(knn_url, emb):
    request_json = {'emb': emb.tolist()}
    knn_resp = requests.post(knn_url, json=request_json).json()
    return knn_resp


@app.post('/')
def endpoint():
    req = request.json
    query = req['query']
    emb = get_text_embedding(text=query)
    cluster_idx = get_closest_cluster_idx(emb=emb)
    knn_worker_url = get_knn_worker_url(cluster_idx=cluster_idx, n_neighbors=10)
    knn_resp = get_knn_resp(knn_url=knn_worker_url, emb=emb)
    return jsonify(knn_resp)


@app.get('/healthcheck')
def healthcheck():
    if not centroids:
        return jsonify({'status': 'centroids itn\'t loaded'}), 504

    return jsonify({'status': 'ok'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
